//khai báo thư viện express
const express = require('express');
const { userMiddleware } = require('../middlewares/userMiddleware');

//tạo router
const userRouter = express.Router();

//sủ dụng middle ware
userRouter.use(userMiddleware);

//get all user
userRouter.get('/user', (request, response) => {
    console.log("Get all courese");
    response.json({
        message:'Get All user'
    })
});

//get a course
userRouter.get('/user/:userId', (request, response) => {
    let id = request.params.userId;

    console.log(`Get All userId = ${id}`);
    response.json({
        message:`Get All userId = ${id}`
    })
})

//create a course
userRouter.post('/user', (request, response) => {
    let body = request.body;

    console.log('create a course');
    console.log(body);
    response.json({
        ...body
    })
});


//update a course
userRouter.put('/user/:userId', (request, response) => {
    let id = request.params.userId;
    let body = request.body;

    console.log('update a course');
    console.log({id, ...body});
    response.json({
        message: {id, ...body}
    })
})

//delete a course
userRouter.delete('/user/:userId', (request, response) => {
    let id = request.params.userId;

    console.log('delete a course' + id);
    response.json({
        message: 'delete a course' + id
    })
})

module.exports = { userRouter };