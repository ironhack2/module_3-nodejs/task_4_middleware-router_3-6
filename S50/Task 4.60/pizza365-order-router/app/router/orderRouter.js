//khai báo thư viện express
const express = require('express');
const { orderMiddleware } = require('../middlewares/orderMiddleware');

//tạo router
const orderRouter = express.Router();

//sủ dụng middle ware
orderRouter.use(orderMiddleware);

//get all order
orderRouter.get('/order', (request, response) => {
    console.log("Get all courese");
    response.json({
        message:'Get All order'
    })
});

//get a course
orderRouter.get('/order/:orderId', (request, response) => {
    let id = request.params.orderId;

    console.log(`Get All orderId = ${id}`);
    response.json({
        message:`Get All orderId = ${id}`
    })
})

//create a course
orderRouter.post('/order', (request, response) => {
    let body = request.body;

    console.log('create a course');
    console.log(body);
    response.json({
        ...body
    })
});


//update a course
orderRouter.put('/order/:orderId', (request, response) => {
    let id = request.params.orderId;
    let body = request.body;

    console.log('update a course');
    console.log({id, ...body});
    response.json({
        message: {id, ...body}
    })
})

//delete a course
orderRouter.delete('/order/:orderId', (request, response) => {
    let id = request.params.orderId;

    console.log('delete a course' + id);
    response.json({
        message: 'delete a course' + id
    })
})

module.exports = { orderRouter };