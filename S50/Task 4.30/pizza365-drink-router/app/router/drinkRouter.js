//khai báo thư viện express
const express = require('express');
const { drinkMiddleware } = require('../middlewares/drinkMiddleware');

//tạo router
const drinkRouter = express.Router();

//sủ dụng middle ware
drinkRouter.use(drinkMiddleware);

//get all drink
drinkRouter.get('/drink', (request, response) => {
    console.log("Get all courese");
    response.json({
        message:'Get All drink'
    })
});

//get a course
drinkRouter.get('/drink/:drinkId', (request, response) => {
    let id = request.params.drinkId;

    console.log(`Get All drinkId = ${id}`);
    response.json({
        message:`Get All drinkId = ${id}`
    })
})

//create a course
drinkRouter.post('/drink', (request, response) => {
    let body = request.body;

    console.log('create a course');
    console.log(body);
    response.json({
        ...body
    })
});


//update a course
drinkRouter.put('/drink/:drinkId', (request, response) => {
    let id = request.params.drinkId;
    let body = request.body;

    console.log('update a course');
    console.log({id, ...body});
    response.json({
        message: {id, ...body}
    })
})

//delete a course
drinkRouter.delete('/drink/:drinkId', (request, response) => {
    let id = request.params.drinkId;

    console.log('delete a course' + id);
    response.json({
        message: 'delete a course' + id
    })
})

module.exports = { drinkRouter };