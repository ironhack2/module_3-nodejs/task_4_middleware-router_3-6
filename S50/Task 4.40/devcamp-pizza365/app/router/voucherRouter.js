//khai báo thư viện express
const express = require('express');
const { voucherMiddleware } = require('../middlewares/voucherMiddleware');

//tạo router
const voucherRouter = express.Router();

//sủ dụng middle ware
voucherRouter.use(voucherMiddleware);

//get all voucher
voucherRouter.get('/voucher', (request, response) => {
    console.log("Get all courese");
    response.json({
        message:'Get All voucher'
    })
});

//get a course
voucherRouter.get('/voucher/:voucherId', (request, response) => {
    let id = request.params.voucherId;

    console.log(`Get All voucherId = ${id}`);
    response.json({
        message:`Get All voucherId = ${id}`
    })
})

//create a course
voucherRouter.post('/voucher', (request, response) => {
    let body = request.body;

    console.log('create a course');
    console.log(body);
    response.json({
        ...body
    })
});


//update a course
voucherRouter.put('/voucher/:voucherId', (request, response) => {
    let id = request.params.voucherId;
    let body = request.body;

    console.log('update a course');
    console.log({id, ...body});
    response.json({
        message: {id, ...body}
    })
})

//delete a course
voucherRouter.delete('/voucher/:voucherId', (request, response) => {
    let id = request.params.voucherId;

    console.log('delete a course' + id);
    response.json({
        message: 'delete a course' + id
    })
})

module.exports = { voucherRouter };