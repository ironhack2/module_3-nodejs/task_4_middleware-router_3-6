//khai báo thư viện express
const express = require('express');
const { courseMiddleware } = require('../middlewares/courseMiddleware');

//tạo router
const courseRouter = express.Router();

//sủ dụng middle ware
courseRouter.use(courseMiddleware);

//get all courses
courseRouter.get('/courses', (request, response) => {
    console.log("Get all courses");
    response.json({
        message: 'Get All Courses'
    })
});

//get a course
courseRouter.get('/courses/:courseId', (request, response) => {
    let id = request.params.courseId;

    console.log(`Get All CourseId = ${id}`);
    response.json({
        message: `Get All CourseId = ${id}`
    })
})

//create a course
courseRouter.post('/courses', (request, response) => {
    let body = request.body;

    console.log('create a course');
    console.log(body);
    response.json({
        ...body
    })
});


//update a course
courseRouter.put('/courses/:courseId', (request, response) => {
    let id = request.params.courseId;
    let body = request.body;

    console.log('update a course');
    console.log({ id, ...body });
    response.json({
        message: { id, ...body }
    })
})

//delete a course
courseRouter.delete('/courses/:courseId', (request, response) => {
    let id = request.params.courseId;

    console.log('delete a course' + id);
    response.json({
        message: 'delete a course' + id
    })
})

module.exports = { courseRouter };